from k_layer_bayesiannetwork import *
import numpy as np
import common as c
import matplotlib.pyplot as plt

# Generate data
rng = np.random.RandomState(1337)
cfg = c.Cfg()
cfg.rng = rng
cfg.numK = 3
# cfg.A = Model.generate_random_weights(rng, cfg.numK)
cfg.A = [np.ones((2))*0.5, np.ones((2, 2))*0.5, np.ones((2, 2, 2))*0.5]
cfg.mu = np.eye(2**cfg.numK).reshape([2 for _ in range(0, cfg.numK)] + [2**cfg.numK])

model0 = Model(cfg)
X = model0.generate_data(10000)

# Train new model
rng = np.random.RandomState()
cfg = c.Cfg()
cfg.rng = rng
cfg.numK = 3
cfg.A = Model.generate_random_weights(rng, cfg.numK)
cfg.mu = np.random.random([2 for _ in range(0, cfg.numK)] + [2**cfg.numK])

model1 = Model(cfg)

numEpochs = 500

log = c.Empty()
log.ll = np.zeros(numEpochs)
log.A = [None] * numEpochs
log.mu = np.zeros((numEpochs, 2**model1.numK, model1.numF))

for iEpoch in range(0, numEpochs):
    log.A[iEpoch] = model1.A
    log.mu[iEpoch] = model1.mu.reshape(log.mu.shape[1:])

    (p, ll) = model1.infer(X)
    log.ll[iEpoch] = ll

    model1.learn(p, X)
    
    
plt.figure()
plt.subplot(2, 1, 1); plt.plot(log.ll); plt.xlim(0, numEpochs)
plt.subplot(2, 1, 2); plt.plot(log.ll[1:] >= log.ll[:-1]); plt.xlim(0, numEpochs)

plt.figure()
for ik in range(0, model1.numK):
    plt.subplot(model1.numK, 1, ik+1)
    
    plotdata = [a[ik].reshape((-1)) for a in log.A]
    plt.plot(plotdata)
    plt.ylim([0, 1])

plt.figure()
plt.pcolormesh(model.mu.reshape((-1, 8)), vmin=0, vmax=1)


