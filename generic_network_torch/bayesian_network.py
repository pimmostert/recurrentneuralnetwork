from abc import abstractmethod
from enum import Enum
from typing import List, Dict

import numpy as np


class BayesianNetwork:
    def __init__(self, nodes: List['Node'], parents: Dict['Node', List['Node']]):
        self.nodes = nodes
        self.parents = parents
        self.num_nodes = len(self.nodes)


class NodeType(Enum):
    CPTNode = 1


class Node:
    @property
    @abstractmethod
    def node_type(self) -> NodeType:
        pass


class CPTNode(Node):
    node_type = NodeType.CPTNode

    def __init__(self, cpt: np.array):
        if not np.all(np.abs(cpt.sum(axis=-1) - 1) < 1e-15):
            raise Exception('The CPT should sum to 1 along the last dimension')

        self.numK = cpt.shape[-1]
        self.cpt = cpt

