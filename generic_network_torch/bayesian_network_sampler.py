from abc import abstractmethod
from typing import List, Dict

import torch
import common as cmn
from generic_network_torch import CPTNode, BayesianNetwork, Node


class BayesianNetworkSampler:
    def __init__(self, bayesian_network: BayesianNetwork):
        self.samplers: Dict[Node, NodeSampler] = {node: NodeSamplers.get_for(node) for node in bayesian_network.nodes}
        self.parents = {node: bayesian_network.parents[node] for node in bayesian_network.nodes}

    def sample(self, num_samples: int, nodes: List[Node]) -> torch.Tensor:
        num_nodes = len(nodes)

        samples = torch.empty((num_samples, num_nodes), device=cmn.torch_device)

        for i_sample in range(num_samples):
            samples[i_sample, :] = self._sample_single_trial(nodes)

        return samples

    def _sample_single_trial(self, nodes: List[Node]) -> torch.tensor:
        states = dict()

        for (i, node) in enumerate(nodes):
            states[node] = self._sample_single_node(node, states)

        return torch.tensor([states[node] for node in nodes], device=cmn.torch_device)

    def _sample_single_node(self, node: Node, states: Dict[Node, torch.tensor]) -> torch.tensor:
        for parent in self.parents[node]:
            if parent not in states:
                states[parent] = self._sample_single_node(parent, states)

        parent_states = torch.tensor([states[parent] for parent in self.parents[node]], device=cmn.torch_device)
        return self.samplers[node].sample(parent_states)


class NodeSamplers:
    factories = {
        CPTNode: lambda node: CPTNodeSampler(node)
    }

    @staticmethod
    def get_for(node) -> 'NodeSampler':
        return NodeSamplers.factories[type(node)](node)


class NodeSampler():
    @abstractmethod
    def sample(self, parents_states: torch.tensor):
        pass


class CPTNodeSampler(NodeSampler):
    def __init__(self, cptnode: CPTNode):
        self.cpt = torch.tensor(cptnode.cpt, device=cmn.torch_device)

    def sample(self, parents_states: torch.tensor) -> torch.tensor:
        p = self.cpt[tuple(parents_states)]

        return torch.multinomial(p, 1, replacement=True)
