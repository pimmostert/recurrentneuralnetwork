from simple_bayesiannetwork import *
import numpy as np
import common as c
import matplotlib.pyplot as plt

# Generate data
cfg = c.Cfg()
cfg.A1 = np.array([0.5, 0.5])
cfg.A2 = np.array([[0.5, 0.5], [0.5, 0.5]])
cfg.A3 = np.array([[[0.5, 0.5], [0.5, 0.5]], [[0.5, 0.5], [0.5, 0.5]]])
cfg.mu = np.eye(8).reshape((2, 2, 2, 8))

model0 = Model(cfg)
X = model0.generate_data(10000)

# Train new model
cfg = c.Cfg()
cfg.A1 = generate_random_probability_matrix((2))
cfg.A2 = generate_random_probability_matrix((2, 2))
cfg.A3 = generate_random_probability_matrix((2, 2, 2))
cfg.mu = np.random.random((2, 2, 2, 8))

model = Model(cfg)

numEpochs = 500

log = c.Empty()
log.ll = np.zeros(numEpochs)
log.A1 = np.zeros((numEpochs, 2))
log.A2 = np.zeros((numEpochs, 2, 2))
log.A3 = np.zeros((numEpochs, 2, 2, 2))
log.mu = np.zeros((numEpochs, 2, 2, 2, 8))

for iEpoch in range(0, numEpochs):
    log.A1[iEpoch, :] = model.A1
    log.A2[iEpoch, :, :] = model.A2
    log.A3[iEpoch, :, :, :] = model.A3
    log.mu[iEpoch, :, :, :, :] = np.copy(model.mu)

    (p, ll) = model.infer(X)
    log.ll[iEpoch] = ll

    model.learn(p, X)
    
    
plt.figure()
plt.subplot(2, 1, 1); plt.plot(log.ll); plt.xlim(0, numEpochs)
plt.subplot(2, 1, 2); plt.plot(log.ll[1:] >= log.ll[:-1]); plt.xlim(0, numEpochs)

plt.figure()
plt.subplot(4, 1, 1); plt.plot(log.A1.reshape((numEpochs, -1))); plt.ylim([0, 1])
plt.subplot(4, 1, 2); plt.plot(log.A2.reshape((numEpochs, -1))); plt.ylim([0, 1])
plt.subplot(4, 1, 3); plt.plot(log.A3.reshape((numEpochs, -1))); plt.ylim([0, 1])
plt.subplot(4, 1, 4); plt.plot(log.mu.reshape((numEpochs, -1))); plt.ylim([0, 1])

plt.figure()
plt.pcolormesh(model.mu.reshape((-1, 8)), vmin=0, vmax=1)


