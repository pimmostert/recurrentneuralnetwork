import numpy as np

class Model:
    def __init__(self, cfg0):        
        self.A1 = cfg0.A1
        self.A2 = cfg0.A2
        self.A3 = cfg0.A3
        self.mu = cfg0.mu

        self.N1 = self.A1
        self.N2 = self.A2 * self.N1[:, None]
        self.N3 = self.A3 * self.N2[:, :, None]

        self.numK = self.mu.shape[-1]
        
    def generate_data(self, numN):
        p1 = np.zeros((numN, 2))
        p2 = np.zeros((numN, 2))
        p3 = np.zeros((numN, 2))
        X = np.zeros((numN, self.numK))
        
        for n in range(0, numN):
            p1_index = draw_random_sample(self.A1)
            p1[n][p1_index] = 1
            
            p2_index = draw_random_sample(self.A2[p1_index, :])
            p2[n][p2_index]= 1

            p3_index = draw_random_sample(self.A3[p1_index, p2_index, :])
            p3[n][p3_index]= 1
            
            X[n] = np.random.rand(self.numK) < self.mu[p1_index, p2_index, p3_index, :]
            
        return X
    
    def log_likelihood(self, x):
        a = np.log(self.mu / (1-self.mu))
        b = np.log(1-self.mu).sum(axis=-1)
        
        return (x[None, None, None, :] * a[:, :, :, :]).sum(axis=3) \
            + b[:, :, :]
        
    def infer(self, x):        
        # P(Q1, Q2, Q3 | x)       - p: [2, 2, 2]
        p = self.A1[:, None, None] \
            * self.A2[:, :, None] \
            * self.A3[:, :, :] \
            * np.exp(self.log_likelihood(x))
            
        c = p.sum()
        p /= c
        
        ll = np.log(c)
        
        return (p, ll)    
        
    def learn(self, p, x, gamma):
        d_A1 = p.sum(axis=(1, 2)) - self.A1
        self.A1 += gamma*d_A1

        d_N1 = p.sum(axis=(1, 2)) - self.N1
        self.N1 += gamma*d_N1

        d_A2 = (p.sum(axis=1) - p.sum(axis=(1, 2))[:, None]*self.A2)/self.N1[:, None]
        self.A2 += gamma*d_A2

        d_N2 = p.sum(axis=2) - self.N2
        self.N2 += gamma*d_N2

        d_A3 = (p - p.sum(axis=2)[:, :, None]*self.A3)/self.N2[:, :, None]
        self.A3 += gamma*d_A3

        d_N3 = p - self.N3
        self.N3 += gamma*d_N3

        d_mu = (p[:, :, :, None] / self.N3[:, :, :, None]) \
            * (x[None, None, None, :] - self.mu)
        self.mu += gamma*d_mu  
        self.mu = self.mu*0.999999 + 0.0000005

    def learn_simplified(self, p, x, gamma):
        d_N1 = p.sum(axis=(1, 2)) - self.N1
        self.N1 += gamma*d_N1

        d_N2 = p.sum(axis=2) - self.N2
        self.N2 += gamma*d_N2

        d_N3 = p - self.N3
        self.N3 += gamma*d_N3

        self.A1 = self.N1
        self.A2 = self.N2 / self.N2.sum(axis=1)[:, None]
        self.A3 = self.N3 / self.N3.sum(axis=2)[:, :, None]

        d_mu = (p[:, :, :, None] / self.N3[:, :, :, None]) \
            * (x[None, None, None, :] - self.mu)
        self.mu += gamma*d_mu  
        self.mu = self.mu*0.999999 + 0.0000005

def draw_random_sample(p):
    return np.random.choice(p.shape[0], p=p)
        
def generate_random_probability_matrix(shape):
    A = np.random.random(shape)
    
    A /= A.sum(axis=-1)[..., None]
    
    return A

    
        
        
        