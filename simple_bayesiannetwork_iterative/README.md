# Simple Bayesian network, iterative learning

Rather than updating the parameters after each epoch by making use of inference over all data, in this experiment the parameters are updated after each trial.

## Model

Same as before:

```mermaid
graph TD
    Q1((Q1))
    Q2((Q2))
    Q3((Q3))
    x((x))

    Q1-->Q2
    Q2-->Q3
    Q3-->x
    
    Q2-->x
    Q1-->x
    Q1-->Q3
```

```math
\begin{equation}
    P(Q^1, Q^2, Q^3, \mathbf{x}) = P(Q^1)P(Q^2|Q^1)P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3)
\end{equation}
```

## Iterative learning

The responsibilities $`\gamma_{nlij}`$ per trial are defined as the posterior distribution in the E-step:

```math
\begin{equation}
    \gamma_{nlij} = P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1 | \mathbf{x}_n)
\end{equation}
```

### $`A^1`$

In batch-wise learning, $`A^1`$ is calculated as follows:

```math
\begin{equation}
    A^1_l = \frac{1}{N} \sum_n \sum_i \sum_j \gamma_{nlij},
\end{equation}
```

where $`N`$ is the total number of data points or trials. 

This calculation may be written in an iterative way over trials. For brevity, let's define $`a_l = A^1_l`$ and $`\rho_{nl} = \sum_{i} \sum_{j} \gamma_{nlij}`$. Now suppose that we calculate $`a_l`$ after the E-step at each trial and label it $`a^n_l`$ for trial $`n`$. Then, the above equation, where $`a_l`$ is calculated after the final trial, may be rewritten as:

```math
\begin{equation}
    a^N_l = \frac{1}{N} \sum_i^N \rho_{il}
\end{equation}
```

This calculation can be rewritten in a recursive way over trials. For any arbitrary trial $`n`$ (except maybe the first):

```math
\begin{equation}
    a^n_l = a^{n-1}_l + \Delta a_l^n
\end{equation}
```

```math
\begin{align}
    \Delta a_l^n & = 
        a^n_l - a^{n-1}_l \\

    & =
        \displaystyle 
        \frac{\displaystyle \sum_i^n \rho_{il}}{n} 
        - \frac{\displaystyle \sum_i^{n-1} \rho_{il}}{n-1} 

    \\

    & =
        \displaystyle 
        \frac{\displaystyle (n-1) \sum_i^n \rho_{il} - n \sum_i^{n-1} \rho_{il}}{n(n-1)} 

    \\

    & =
        \displaystyle 
        \frac{\displaystyle (n-1) \sum_i^n \rho_{il} - ((n-1)+1) \sum_i^{n-1} \rho_{il}}{n(n-1)} 

    \\

    & =
        \displaystyle 
        \frac{
            \displaystyle 
            (n-1) \sum_i^n \rho_{il} 
            - (n-1) \sum_i^{n-1} \rho_{il}
            - \sum_i^{n-1} \rho_{il}
        }
        {n(n-1)} 
        
    \\

    & =
        \displaystyle 
        \frac{
            \displaystyle 
            (n-1) 
            \left ( 
                \sum_i^n \rho_{il} 
                - \sum_i^{n-1} \rho_{il}
            \right )
            - \sum_i^{n-1} \rho_{il}
        }
        {n(n-1)} 
        
    \\

    & =
        \displaystyle 
        \frac{
            \displaystyle 
            (n-1) 
            \rho_{nl}                 
            - \sum_i^{n-1} \rho_{il}
        }
        {n(n-1)} 
        
    \\

    & =
        \displaystyle 
        \frac{
            \displaystyle 
            \rho_{nl}                 
            - \frac{ \sum_i^{n-1} \rho_{il} }{n-1}
        }
        {n} 
        
    \\

    & =
        \displaystyle 
        \frac{
            \displaystyle 
            \rho_{nl}                 
            - a^{n-1}_l
        }
        {n} 
        
    \\

    & =
        \displaystyle 
        \frac{1}{n}
        (
            \rho_{nl}                 
            - a^{n-1}_l
        ) 
\end{align}
```

Thus, $`A^1`$ may be calculated iteratively, updated after each trial according to the above equations. 

### $`A^2`$

Similarly to $`A1`$, $`A2`$ may be calculated in an iterative manner over trials as well.

In batch-wise learning:
```math
\begin{equation}
    A^2_{li} = 
        \frac
        {
            \displaystyle \sum_n \sum_j \gamma_{nlij}
        }
        {
            \displaystyle \sum_n \sum_j \sum_{i'} \gamma_{nli'j}
        }
\end{equation}
```
Now, as before, for brevity let's define $`b_{li} = A^2_{li}`$ and $`\psi_{nli} = \sum_j \gamma_{nlij}`$. The above equation can be rewritten as:

```math
\begin{equation}
    b_{li}^N = 
        \frac
        {
            \displaystyle \sum_e^N \psi_{eli}
        }
        { \displaystyle Z^N_l }
\end{equation}
```
calculated after the last trial, with

```math
\begin{equation}
    Z^n_l = \sum_e^n \sum_{i'} \psi_{eli'}
\end{equation}
```

Then we can calculate $`b_{li}^n`$ too for any arbitrary trial $`n`$ in a recursive manner:

```math
\begin{align}
    b^n_{li} & = 
        b^{n-1}_{li} + \Delta b^n_{li}
    \\

    \Delta b^n_{li} & =
        b^n_{li} - b^{n-1}_{li}
    \\

    & =
        \frac
        {
            \displaystyle \sum_e^n \psi_{eli}
        }
        { \displaystyle Z^n_l }
        - \frac
        {
            \displaystyle \sum_e^{n-1} \psi_{eli}
        }
        { \displaystyle Z^{n-1}_l }
    \\

    & = 
        \frac
        {
            \displaystyle 
            Z^{n-1}_l \sum_e^n \psi_{eli}
            - Z^n_l \sum_e^{n-1} \psi_{eli}
        }
        { 
            \displaystyle Z^n_lZ^{n-1}_l 
        }
    \\
\end{align}
```

Now, note that 
```math
\begin{align}
    Z^n_l & = 
        \sum_e^n \sum_{i'} \psi_{eli'} 
    \\

    & = 
        \sum_e^{n-1} \sum_{i'} \psi_{eli'} + \sum_{i'} \psi_{nli'} 
    \\
    
    & = 
    Z^{n-1}_l + \sum_{i'} \psi_{nli'}
\end{align}
```
and so:

```math
\begin{align}
    ... & = 
        \frac
        {
            \displaystyle 
            Z^{n-1}_l \sum_e^n \psi_{eli}
            - (
                Z^{n-1}_l + \sum_{i'} \psi_{nli'}
            ) 
            \sum_e^{n-1} \psi_{eli}
        }
        { 
            \displaystyle Z^n_lZ^{n-1} _l
        }
    \\

    & =
        \frac
        {
            \displaystyle 
            Z^{n-1}_l \sum_e^n \psi_{eli}
            -  Z^{n-1}_l \sum_e^{n-1} \psi_{eli}
            - \sum_{i'} \psi_{nli'} \sum_e^{n-1} \psi_{eli}
        }
        { 
            \displaystyle Z^n_lZ^{n-1} _l
        }
    \\

    & =
        \frac
        {
            \displaystyle         
            Z^{n-1}_l 
            ( 
                \sum_e^n \psi_{eli}
                - \sum_e^{n-1} \psi_{eli}
            )
            - \sum_{i'} \psi_{nli'} \sum_e^{n-1} \psi_{eli}
        }
        { 
            \displaystyle Z^n_lZ^{n-1} _l
        }
    \\

    & =
        \frac
        {
            \displaystyle         
            Z^{n-1}_l \psi_{nli}
            - \sum_{i'} \psi_{nli'} \sum_e^{n-1} \psi_{eli}
        }
        { 
            \displaystyle Z^n_lZ^{n-1}_l
        }
    \\

    & =
        \displaystyle \frac {1}{Z^n_l}
        \left ( 
            \frac
            {
                \displaystyle         
                Z^{n-1}_l \psi_{nli}
            }
            { Z^{n-1}_l }
            - \frac
            {
                \displaystyle         
                \sum_{i'} \psi_{nli'} \sum_e^{n-1} \psi_{eli}
            }
            { Z^{n-1}_l }
        \right )
    \\

    & =
        \displaystyle \frac {1}{Z^n_l}
        \left ( 
            \psi_{nli}
            -
            \sum_{i'} \psi_{nli'} 
            b^{n-1}_{li}
        \right )
    \\
\end{align}
```

### $`A^3`$

$`A3`$ can be calculated in an iterative way analogously to $`A2`$.

Let's define $`c^N_{lij} = A^3_{lij}`$ where $`c^n_{lij}`$ is calculated after the $`n`$'th trial. Then:

```math
\begin{align}
    c^n_{lij} & =
        c^{n-i}_{lij} + \Delta c^n_{lij}
    \\

    \Delta c^n_{lij} & =
        \displaystyle \frac {1}{U^n_{li}}
        \left ( 
            \gamma_{nlij}
            -
            \sum_{j'} \gamma_{nlij'} 
            c^{n-1}_{lij}
        \right )
    \\

    U^n_{li} & =
        \displaystyle
        U^{n-1}_{li} + \sum_{j'} \gamma_{nlij'}

\end{align}
```

### $`\mu`$

In batchwise-learning:

```math
\begin{align}
    \mu_{lijk} & =
        \frac
        {
            \displaystyle
            \sum_n \gamma_{nlij} x_{nk}
        }
        { 
            \displaystyle
            N_{lij} 
        }
    \\

    N_{lij} & =
        \displaystyle
        \sum_n \gamma_{nlij}
\end{align}
```

When calculated after any arbitrary trial $`n`$:

```math
\begin{align}
    \mu^n_{lijk} & =
        \frac
        {
            \displaystyle
            \sum^n_e \gamma_{elij} x_{ek}
        }
        { 
            \displaystyle
            N^n_{lij} 
        }
    \\

    N^n_{lij} & =
        \displaystyle
        \sum^n_e \gamma_{elij}
\end{align}
```

To calculate iteratively:

```math
\begin{align}
    N^n_{lij} & =
        \displaystyle
        \sum^n_e \gamma_{elij}
    \\

    & =
        \displaystyle
        \sum^{n-1}_e \gamma_{elij}
        + \gamma_{nlij}
    \\

    & =
        \displaystyle
        N^{n-1}_{lij}
        + \gamma_{nlij}
\end{align}
```

```math
\begin{align}
    \mu^n_{lijk} & =
        \mu^{n-1}_{lijk} + \Delta \mu^n_{lijk}
    \\

    \Delta \mu^n_{lijk} & =
        \mu^n_{lijk} - \mu^{n-1}_{lijk}
    \\

    & =
        \frac
        {
            \displaystyle
            \sum^n_e \gamma_{elij} x_{ek}
        }
        { 
            \displaystyle
            N^n_{lij} 
        }
        -
        \frac
        {
            \displaystyle
            \sum^{n-1}_e \gamma_{elij} x_{ek}
        }
        { 
            \displaystyle
            N^{n-1}_{lij} 
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} \sum^n_e \gamma_{elij} x_{ek}
            -
            N^n_{lij} \sum^{n-1}_e \gamma_{elij} x_{ek}
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} \sum^n_e \gamma_{elij} x_{ek}
            -
            ( N^{n-1}_{lij} + \gamma_{nlij} ) 
            \sum^{n-1}_e \gamma_{elij} x_{ek}
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} \sum^n_e \gamma_{elij} x_{ek}
            -
            N^{n-1}_{lij} \sum^{n-1}_e \gamma_{elij} x_{ek}
            - \gamma_{nlij} \sum^{n-1}_e \gamma_{elij} x_{ek} 
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} 
            (
                \sum^n_e \gamma_{elij} x_{ek}
                -
                \sum^{n-1}_e \gamma_{elij} x_{ek}
            )
            - \gamma_{nlij} \sum^{n-1}_e \gamma_{elij} x_{ek} 
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} 
            \gamma_{nlij} x_{nk}
            - \gamma_{nlij} \sum^{n-1}_e \gamma_{elij} x_{ek} 
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            N^{n-1}_{lij} 
            \gamma_{nlij} x_{nk}
            - \gamma_{nlij} \sum^{n-1}_e \gamma_{elij} x_{ek} 
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \frac
        {
            \displaystyle
            \gamma_{nlij}
            (
                N^{n-1}_{lij} 
                x_{nk}
                - \sum^{n-1}_e \gamma_{elij} x_{ek} 
            )
        }
        { 
            \displaystyle
            N^n_{lij} N^{n-1}_{lij}
        }
    \\

    & =
        \displaystyle \frac
        {
            \gamma_{nlij}
        }
        {
            N^n_{lij}
        }
        \left (
            \frac
            {
                N^{n-1}_{lij} x_{nk}
            }
            { 
                N^{n-1}_{lij}
            }
            -
            \frac
            {
                \displaystyle \sum^{n-1}_e \gamma_{elij} x_{ek} 
            }
            { 
                N^{n-1}_{lij}
            }
        \right )
    \\

    & =
        \displaystyle \frac
        {
            \gamma_{nlij}
        }
        {
            N^n_{lij}
        }
        \left (
            x_{nk}
            -
            \mu^{n-1}_{lijk}
        \right )
    \\
\end{align}
```

## Learning with an indefinite number of trials

The above equations are applicable to finite-sized batches and are equivalent to the 'normal' batch calculations. However, they can be adapted so that they can also operate on an indefinite number of trials.

Consider, for instance the equation to update $`a_l^n`$:

```math
\begin{align}
    \Delta a_l^n =
        \displaystyle 
        \frac{1}{n}
        (
            \rho_{nl}                 
            - a^{n-1}_l
        ) 
\end{align}
```

Note that the number of trials $`n`$ increases throughout this process, and hence the update $`\Delta a^n_l`$ gets progressively smaller. In a batch-wise scenario, i.e. a finite number of trials, this makes sense. It represents the weight of a new data point relative to the larger body of already processed data. However, in online learning, i.e. an indefinite number of trials, $`n`$ may be seen as a learning rate. It could be set to a fixed value to define the smoothing window by which old trials are gradually forgotten.

Similarly, consider the equation to update $`\mu^n_{lijk}`$:

```math
\begin{align}
    \Delta \mu^n_{lijk} & =
        \displaystyle \frac
        {
            \gamma_{nlij}
        }
        { \displaystyle N^n_{lij} }
        \left (
            x_{nk}
            -
            \mu^{n-1}_{lijk}
        \right )
    \\
\end{align}
```

Here however, $`N^n_{lij}`$ is dependent on $`l`$, $`i`$ and $`j`$ rather then being the absolute number of trials. Instead, it may be considered a pseudo-count of the number of times that trial $`n`$ was drawn via path $`(q^1_l = 1, q^2_i = 1, q^3_j = 1)`$. The fact that this pseudo-count is dependent on its indices indicates that we can not simply replace it by a single fixed learning rate parameter as above.

We can resolve this problem by redefining the pseudo-count in terms of the absolute number of trials and the average count:

```math
\begin{align}
    N^n_{lij} = n \bar{N}^n_{lij}
\end{align}
```

Which may be calculated iteratively:

```math
\begin{align}
    \bar{N}^n_{lij} & =
        \bar{N}^{n-1}_{lij} + \Delta \bar{N}^n_{lij}
    \\

    \Delta \bar{N}^n_{lij} & =
        \bar{N}^n_{lij} - \bar{N}^{n-1}_{lij}
    \\

    & = ...
    \\ 

    & =
        \frac{1}{n}(\gamma_{nlij} - \bar{N}^{n-1}_{lij})
\end{align}
```

Now $`\Delta \mu^n_{lijk}`$ can also be updated with $`n`$ being considered a learning parameter. Note that $`\bar{N}^N_{lij}`$ represents $`P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1)`$.

Similar derivations can be made for $`\Delta b^n_{li}`$ ($`A2`$):

```math
\begin{align}
    \bar{Z}^n_{l} & = 
        \bar{Z}^{n-1}_{l} + \Delta \bar{Z}^n_{l}
    \\

    \Delta \bar{Z}^n_{l} & = 
        \bar{Z}^n_{l} - \bar{Z}^{n-1}_{l}
    \\

    & =
        ...
    \\

    & =
        \frac{1}{n}
        \left (
            \sum_j \sum_{i} \gamma_{nlij} - \bar{Z}^{n-1}_{l}
        \right )
\end{align}
```

and for $`\Delta c^n_{lij}`$ ($`A3`$):

```math
\begin{align}
    \bar{U}^n_{li} & = 
        \bar{U}^{n-1}_{li} + \Delta \bar{U}^n_{li}
    \\

    \Delta \bar{U}^n_{li} & = 
        \bar{U}^n_{li} - \bar{U}^{n-1}_{li}
    \\

    & =
        ...
    \\

    & =
        \frac{1}{n}
        \left (
            \sum_j \gamma_{nlij} - \bar{U}^{n-1}_{li}
        \right )
\end{align}
```

## Putting it together

Summarizing, these calculations intend to find a way to update the model iteratively after each trial, performing both an E- and an M-step. It should handle an indefinite number of trials, hence it should operate as a "moving average" whereby the learning parameter determines how rapidly previous trials are forgotten.

First, define and initialize parameters:
```math
\begin{align}
    A^1_l & \equiv 
        P(Q^1_l = 1)
    \\

    N^1_l & \equiv
        P(Q^1_l = 1)
    \\

    & = 
        A^1_l
    \\
\end{align}
```

```math
\begin{align}
    A^2_{li} & \equiv 
        P(Q^2_i = 1 | Q^1_l = 1)
    \\

    N^2_{li} & \equiv
        P(Q^1_l = 1, Q^2_i = 1)
    \\

    & =
        A^2_{li} \cdot N^1_l
    \\
\end{align}
```

```math
\begin{align}
    A^3_{lij} & \equiv 
        P(Q^3_j = 1 | Q^1_l = 1, Q^2_i = 1)
    \\

    N^3_{lij} & \equiv
        P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1)
    \\

    & =
        A^3_{lij} \cdot N^2_{li}
    \\
\end{align}
```

```math
\begin{align}
    \mu_{lijk} & \equiv
        P(x_k = 1| Q^1_l=1, Q^2_i=1, Q^3_j=1)
\end{align}
```

For each trial, calculate responsibilities:

```math
\begin{align}
    \gamma_{lij} \equiv P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1 | x)
\end{align}
```

Then update parameters:

```math
\begin{align}
    (A^1_l)^\text{new} & =
        \eta
        \left (
            \sum_j \sum_i \gamma_{lij}
            - (A^1_l)^\text{old}
        \right ) 
    \\

    (N^1_{l})^\text{new} & =
        \eta 
        \left (
            \sum_j \sum_i \gamma_{lij} - (N^1_{l})^\text{old}
        \right )
    \\

    (A^2_{li})^\text{new} & =
        \eta
        \frac{1}{(N^1_l)^\text{new}}
        \left (
            \sum_j \gamma_{lij}
            - (\sum_{i'} \sum_j \gamma_{li'j}) (A^2_{li})^\text{old}
        \right ) 
    \\

    (N^2_{li})^\text{new} & =
        \eta 
        \left (
            \sum_j \gamma_{lij} - (N^2_{li})^\text{old}
        \right )
    \\

    (A^3_{lij})^\text{new} & =
        \eta
        \frac{1}{(N^2_{li})^\text{new}}
        \left (
            \gamma_{lij}
            - (\sum_{j'} \gamma_{lij'}) (A^3_{lij})^\text{old}
        \right ) 
    \\

    (N^3_{lij})^\text{new} & =
        \eta 
        \left (
            \gamma_{lij} - (N^3_{lij})^\text{old}
        \right )
    \\

    (\mu_{lijk})^\text{new} & =
        \eta
        \frac { \gamma_{lij} } { (N^3_{lij})^\text{new} }
        \left (
            x_{k}
            - (\mu_{lijk})^\text{old}
        \right ) 
    \\
\end{align}
```

## Simplifying further
It seems that the above update equations can be simplified:

```math
\begin{align}
    (N^1_{l})^\text{new} & =
        \eta 
        \left (
            \sum_j \sum_i \gamma_{lij} - (N^1_{l})^\text{old}
        \right )
    \\

    (A^1_l)^\text{new} & =
        (N^1_{l})^\text{new}
    \\

    (N^2_{li})^\text{new} & =
        \eta 
        \left (
            \sum_j \gamma_{lij} - (N^2_{li})^\text{old}
        \right )
    \\

    (A^2_{li})^\text{new} & =
        \frac
        {
            (N^2_{li})^\text{new}
        }
        {
            \displaystyle 
            \sum_{i'} (N^2_{li'})^\text{new}            
        }
    \\

    (N^3_{lij})^\text{new} & =
        \eta 
        \left (
            \gamma_{lij} - (N^3_{lij})^\text{old}
        \right )
    \\

    (A^3_{lij})^\text{new} & =
        \frac
        {
            (N^3_{lij})^\text{new}    
        }
        {
            \displaystyle
            \sum_{j'} (N^3_{lij'})^\text{new}
        }
    \\

    (\mu_{lijk})^\text{new} & =
        \eta
        \frac { \gamma_{lij} } { (N^3_{lij})^\text{new} }
        \left (
            x_{k}
            - (\mu_{lijk})^\text{old}
        \right ) 
    \\
\end{align}
```


\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
a