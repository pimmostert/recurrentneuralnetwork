import common as c
import torch


class Model:
    def __init__(self, cfg0):        
        self.A1 = cfg0.A1
        self.A2 = cfg0.A2
        self.A3 = cfg0.A3
        self.mu = cfg0.mu

        self.numK = self.mu.shape[-1]
        
    def generate_data(self, numN):
        p1 = torch.zeros((numN, 2), device=c.torch_device)
        p2 = torch.zeros((numN, 2), device=c.torch_device)
        p3 = torch.zeros((numN, 2), device=c.torch_device)
        X = torch.zeros((numN, self.numK), device=c.torch_device)
        
        for n in range(0, numN):
            p1_index = draw_random_sample(self.A1)
            p1[n][p1_index] = 1
            
            p2_index = draw_random_sample(self.A2[p1_index, :].squeeze())
            p2[n][p2_index] = 1

            p3_index = draw_random_sample(self.A3[p1_index, p2_index, :].squeeze())
            p3[n][p3_index] = 1
            
            X[n] = torch.rand(self.numK) < self.mu[p1_index, p2_index, p3_index, :].squeeze()
            
        return X
    
    def log_likelihood(self, X):
        a = torch.log(self.mu / (1-self.mu))
        b = torch.log(1-self.mu).sum(axis=-1)
        
        return (X[:, None, None, None, :] * a[None, :, :, :, :]).sum(axis=-1) \
            + b[None, :, :, :]
        
    def infer(self, X):        
        numN = X.shape[0]
        
        # P(Q1, Q2, Q3 | x)       - p: [numN, 2, 2, 2]
        p = self.A1[None, :, None, None] \
            * self.A2[None, :, :, None] \
            * self.A3[None, :, :, :] \
            * torch.exp(self.log_likelihood(X))
            
        c = p.reshape((numN, -1)).sum(axis=-1)
        p /= c[:, None, None, None]
        
        ll = torch.log(c).mean()
        
        return p, ll
        
    def learn(self, p, X):
        numN = X.shape[0]
        
        mu = p[:, :, :, :, None] * X[:, None, None, None, :]
        mu = mu.sum(axis=0) / p.sum(axis=0)[:, :, :, None]
        self.mu = mu * 0.999999 + 0.0000005
        
        A1 = p.sum(axis=0) / numN
        A1 = A1.sum(axis=(1, 2))
        self.A1 = A1

        A2 = p.sum(axis=0) / numN
        A2 = A2.sum(axis=2)
        A2 /= A2.sum(axis=1)[:, None]
        self.A2 = A2

        A3 = p.sum(axis=0) / numN
        A3 /= A3.sum(axis=2)[:, :, None]
        self.A3 = A3    
                
def draw_random_sample(p):
    return torch.multinomial(p, 1, replacement=True)
        
def generate_random_probability_matrix(shape):
    A = torch.rand(shape, device=c.torch_device)
    
    A /= A.sum(dim=-1)[..., None]
    
    return A

    
        
        
        