# Simple recurrent neural network, multiple hidden nodes
## Model

```mermaid
 graph LR
    x((x))

    subgraph hidden
        h1((h1))
        h2((h2))
        h_(("h..."))
        h_K(("h_K"))
    end
    
    y((y))

    h1 -- v11 -->h1
    h2 -- v12 -->h1
    h2 -- vK2 -->h_K

    x --w1--> h1
    x --w2--> h2
    x --"w..."--> h_
    x --w_K--> h_K
    
    h1 --u1-->y
    h2 --u2-->y
    h_ --"u..."-->y
    h_K --u_K-->y
```

```math 
\begin{align}
    a^t_k & = w_kx^t + \sum_{k'} v_{k'k} h^{t-1}_{k'} + b_k \\
    a^0_k & = w_kx^0 + b_k \\
    h^t_k & = \sigma(a^t_k) \\ 
    \hat{y}^t & = \sum_{k} u_k h^t_k + d
\end{align}
```

where $`\sigma`$ is the sigmoid function.

## Loss

Loss is defined as the mean squared error over all time samples.

```math 
\begin{align}
    L & = \frac{1}{T} \sum_{t=0}^T L^t \\
    L^t & = \frac{1}{2}(y^t - \hat{y}^t)^2
\end{align}
```

## Gradients
### $`d`$
<hr>

```math
\begin{align}
    \frac{\partial L^t}{\partial d} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial d} \\
    & = -(y^t - \hat{y}^t)
\end{align}
```

### $`u_i`$
<hr>

```math
\begin{align}
    \frac{\partial L^t}{\partial u_i} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial u_i} \\
    & = -(y^t - \hat{y}^t) h^t_i
\end{align}
```

### $`b_i`$
<hr>

```math 
\begin{align}
    \frac{\partial L^t}{\partial b_i} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial b_i} \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k \frac{\partial}{\partial b_i}(u_kh^t_k) \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial b_i} \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial a_k} \cdot \frac{\partial a^t_k}{\partial b_i}
\end{align}
```

$`a^t_k`$ is dependent on $`b_i`$ both via $`b_k`$ itself (if $`k=i`$) as well as through $`h^{t-1}_k`$. Therefore:

```math 
\begin{align}
    \frac{\partial a^t_k}{\partial b_i} & = \sum_{k'} \frac{\partial}{\partial b_i}(v_{k'k} h^{t-1}_{k'}) + \frac{\partial b_k}{\partial b_i} \\
    \frac{\partial a^t_k}{\partial b_i} & = \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial b_i} + \frac{\partial b_k}{\partial b_i}
\end{align}
```

Putting it together:
```math 
\begin{align}
    \frac{\partial L^t}{\partial b_i} & = -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial b_i} \\
    \frac{\partial h^t_k}{\partial b_i} & = \sigma'(a^t_k) 
        \left[ 
            \sum_{k'}v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial b_i} + \frac{\partial b_k}{\partial b_i}
        \right] \\
    \frac{\partial h^0_k}{\partial b_i} & = \sigma'(a^0_k) \frac{\partial b_k}{\partial b_i}
\end{align}
```

### $`w_i`$
<hr>

Analogously to $`b_i`$:

```math 
\begin{align}
    \frac{\partial L^t}{\partial w_i} & = -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial w_i} \\
    \frac{\partial h^t_k}{\partial w_i} & = \sigma'(a^t_k) 
        \left[ 
            \sum_{k'}v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial w_i} + x^t \frac{\partial w_k}{\partial w_i}
        \right] \\
    \frac{\partial h^0_k}{\partial w_i} & = \sigma'(a^0_k) x^0 \frac{\partial w_k}{\partial w_i}
\end{align}
```

### $`v_{ji}`$
<hr>

Analogously to $`b_i`$ and $`w_i`$:

```math 
\begin{align}
    \frac{\partial L^t}{\partial v_{ji}} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial v_{ji}} \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial a_k} \cdot \frac{\partial a^t_k}{\partial v_{ji}}
\end{align}
```

However calculation of $`\frac{\partial a^t}{\partial v_{ji}}`$ now requires the product rule:

```math 
\begin{align}
    \frac{\partial a^t}{\partial v_{ji}} = & 
        \sum_{k'} \frac{\partial}{\partial v_{ji}} ( v_{k'k} h^{t-1}_{k'} )
    \\
    \frac{\partial a^t}{\partial v_{ji}} = & 
        \sum_{k'} 
        \left [
            v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{ji}} 
            + \frac{\partial v_{k'k}}{\partial v_{ji}} h^{t-1}_{k'}
        \right ]
    \\
\end{align}
```

Together:

```math 
\begin{align}
    \frac{\partial L^t}{\partial v_{ji}} & = 
        -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial v_{ji}} 
    \\
    \frac{\partial h^t_k}{\partial v_{ji}} & =
        \sigma'(a^t_k)
        \sum_{k'} 
        \left [
            v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{ji}} 
            + \frac{\partial v_{k'k}}{\partial v_{ji}} h^{t-1}_{k'}
        \right ]
    \\
    \frac{\partial h^0_k}{\partial v_{ji}} & =
        \sigma'(a^t_k) \cdot 0 = 0
    \\
\end{align}
```

Where:

```math 
\begin{align}
    \frac{\partial v_{k'k}}{\partial v_{ji}} = 
    \begin{cases}
        1, & \text{if } j = k' \text{ and } i = k \\
        0, & \text{otherwise}
    \end{cases}
\end{align}
```
