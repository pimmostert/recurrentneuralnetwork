import numpy as np
import matplotlib.pyplot as plt
from common import *

def run(cfg0, x, y):    
    """
    cfg0:
        - w0, b0, v0, u0, d0: initial parameters
        - numT: number of time steps in the time series
        - numN: number of epochs
        - numK: number of hidden nodes
        - alpha: learning rate
        - fix_v: whether v should be trained or not
        - feedback_epoch: provide feedback after so many epochs, or None
    """
    
    w = cfg0.w0 if hasattr(cfg0, 'w0') else np.random.randn(cfg0.numK)
    b = cfg0.b0 if hasattr(cfg0, 'b0') else np.random.randn(cfg0.numK)
    u = cfg0.u0 if hasattr(cfg0, 'u0') else np.random.randn(cfg0.numK)
    d = cfg0.d0 if hasattr(cfg0, 'd0') else np.random.randn(1)
    v = cfg0.v0 if hasattr(cfg0, 'v0') else np.random.randn(cfg0.numK, cfg0.numK) / cfg0.numK   
    
    cfg0.ensure('fix_v', False)
        
    log = Empty()
    log.y_hat = np.zeros([cfg0.numN, cfg0.numT])
    log.w = np.zeros([cfg0.numN, cfg0.numK])
    log.b = np.zeros([cfg0.numN, cfg0.numK])
    log.v = np.zeros([cfg0.numN, cfg0.numK, cfg0.numK])
    log.u = np.zeros([cfg0.numN, cfg0.numK])
    log.d = np.zeros([cfg0.numN])
    log.mse = np.zeros([cfg0.numN])
    
    M = get_M(cfg0.numK)
    
    for n in range(0, cfg0.numN):
        # Forward
        a = np.zeros([cfg0.numT, cfg0.numK])
        h = np.zeros([cfg0.numT, cfg0.numK])
        y_hat = np.zeros([cfg0.numT])
        
        a[0, :] = w*x[0] + b
        h[0, :] = sigmoid(a[0, :])
        y_hat[0] = (u*h[0, :]).sum() + d
            
        for it in range(1, cfg0.numT):
            a[it, :] = w*x[it] + (v*h[it-1, :][:, None]).sum(axis=0) + b
            h[it, :] = sigmoid(a[it, :])
            y_hat[it] = (u*h[it, :]).sum() + d
          
        # Backward        
        dh_dv = np.zeros([cfg0.numT, cfg0.numK, cfg0.numK, cfg0.numK])      # h_k x w_j x w_i
        dh_dw = np.zeros([cfg0.numT, cfg0.numK, cfg0.numK])                 # h_k x w_i
        dh_db = np.zeros([cfg0.numT, cfg0.numK, cfg0.numK])                 # h_k x b_i
        
        dh_db[0, :, :] = dsigmoid(a[0, :])[:, None] * np.eye(cfg0.numK)
        dh_dw[0, :, :] = dsigmoid(a[0, :])[:, None] * np.eye(cfg0.numK) * x[0]
        dh_dv[0, :, :, :] = 0
        for it in range(1, cfg0.numT):
            dh_db[it, :, :] = dsigmoid(a[it, :])[:, None] \
                * ((v[:, :, None] * dh_db[it-1, :, :][:, None, :]).sum(axis=0) + np.eye(cfg0.numK))
            dh_dw[it, :, :] = dsigmoid(a[it, :])[:, None] \
                * ((v[:, :, None] * dh_dw[it-1, :, :][:, None, :]).sum(axis=0) + x[it]*np.eye(cfg0.numK))
            dh_dv[it, :, :, :]  = dsigmoid(a[it, :])[:, None, None] \
                * ( \
                    v[:, :, None, None] * dh_dv[it-1, :, :, :][:, None, :, :] \
                    + M*h[it-1, :][:, None, None, None] \
                ).sum(axis=0)
            
        delta = -(y - y_hat)
            
        db_t = (u[None, :, None] * dh_db).sum(axis=1) * delta[:, None]
        dw_t = (u[None, :, None] * dh_dw).sum(axis=1) * delta[:, None]
        dv_t = (u[None, :, None, None] * dh_dv).sum(axis=1) * delta[:, None, None]
        du_t = delta[:, None] * h
        dd_t = delta
        
        db = db_t.sum(axis=0) / cfg0.numT
        dw = dw_t.sum(axis=0) / cfg0.numT
        dv = dv_t.sum(axis=0) / cfg0.numT
        du = du_t.sum(axis=0) / cfg0.numT
        dd = dd_t.sum(axis=0) / cfg0.numT
        
        # Learn
        w -= cfg0.alpha*dw
        b -= cfg0.alpha*db
        u -= cfg0.alpha*du
        d -= cfg0.alpha*dd
        
        if (not cfg0.fix_v):
            v -= cfg0.alpha*dv
    
        # Log
        log.w[n, :] = w
        log.v[n, :, :] = v
        log.u[n, :] = u
        log.b[n, :] = b
        log.d[n] = d
        log.y_hat[n] = y_hat
        log.mse[n] = (delta**2).sum() / cfg0.numT
        
        if ((cfg0.feedback_epoch is not None) and (n % cfg0.feedback_epoch) == 0):
            print(f'Finished {n}/{cfg0.numN}')
            
    return log
    
def get_M(numK):
    M = np.zeros((numK, numK, numK, numK))
    
    for kprime in range(0, numK):
        for k in range(0, numK):
            for j in range(0, numK):
                for i in range(0, numK):
                    if (j == kprime and i == k):
                        M[kprime, k, j, i] = 1
    
    return M
    
    
    
    
    
    
    
    
    