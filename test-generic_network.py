import numpy as np
import torch

import generic_network_torch as bn

# True network
from generic_network_torch import BayesianNetworkSampler
from generic_network_torch.bayesian_network_em_optimizer import BayesianNetworkEmOptimizer
from generic_network_torch.bayesian_network_inference_machine_naive import BayesianNetworkInferenceMachineNaive

node0_1 = bn.CPTNode(np.array([1/5, 4/5], dtype=np.float64))
node0_2 = bn.CPTNode(np.array([[0.2, 0.8], [0.3, 0.7]], dtype=np.float64))
node0_3 = bn.CPTNode(np.array([[[1, 0, 0, 0], [0, 1, 0, 0]], [[0, 0, 1, 0], [0, 0, 0, 1]]], dtype=np.float64))

nodes0 = [node0_1, node0_2, node0_3]
parents0 = {
    node0_1: [],
    node0_2: [node0_1],
    node0_3: [node0_1, node0_2]}
network0 = bn.BayesianNetwork(nodes0, parents0)

# Sample
num_samples = 1000

sampler = BayesianNetworkSampler(network0)
samples = sampler.sample(num_samples, [node0_1, node0_2, node0_3])

# Optimize
node1_1 = bn.CPTNode(np.array([0.5, 0.5], dtype=np.float64))
node1_2 = bn.CPTNode(np.array([[0.5, 0.5], [0.5, 0.5]], dtype=np.float64))
node1_3 = bn.CPTNode(np.ones((2, 2, 4), dtype=np.float64)/4)

nodes1 = [node1_1, node1_2, node1_3]
parents1 = {
    node1_1: [],
    node1_2: [node1_1],
    node1_3: [node1_1, node1_2]}
network1 = bn.BayesianNetwork(nodes1, parents1)

optimizer = BayesianNetworkEmOptimizer(
    network1,
    lambda bayesian_network: BayesianNetworkInferenceMachineNaive(bayesian_network, [node1_3]))

evidence = samples[:, 2].reshape([num_samples, 1]).int()

num_iterations = 20
optimizer.optimize(
    evidence,
    num_iterations,
    lambda ll, iteration: print(f'Finished iteration {iteration}/{num_iterations} - ll: {ll}'))

pass