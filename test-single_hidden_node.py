from multiple_inputs import *
import numpy as np
import common as c
import matplotlib.pyplot as plt

numJ = 5
numT = 100

data = np.sin(np.arange(0, numT+numJ)*(2*np.pi*11)/(numT+numJ))
x = np.zeros((numT, numJ))

y = data[numJ:]
for ij in range(0, numJ):
    x[:, ij] = data[ij:-(numJ-ij)]

plt.figure()
plt.subplot(2, 1, 1); plt.plot(x, color='black')
plt.subplot(2, 1, 2); plt.plot(y, color='red')


cfg = c.Cfg()
cfg.numJ = numJ
cfg.numK = 5
cfg.numT = numT

network = Network(cfg)

exp = Experiment(network)

numN = 1500
cfg = c.Cfg()
cfg.numN = numN
cfg.alpha = 0.3
cfg.feedback_epoch = 250

log = exp.run(cfg, x, y)

mse = [t.update.mse for t in log.trials]

plt.figure()
plt.plot(np.log(mse))

y_hat = log.trials[-1].forward.y_hat
plt.figure()
plt.plot(y)
plt.plot(y_hat)

w = np.array([t.update.w for t in log.trials])
v = np.array([t.update.v for t in log.trials])
u = np.array([t.update.u for t in log.trials])
b = np.array([t.update.b for t in log.trials])
d = np.array([t.update.d for t in log.trials])

plt.figure(figsize=(10, 15))
plt.subplot(5, 1, 1); plt.plot(w.reshape((numN, -1))); plt.title("w");
plt.subplot(5, 1, 2); plt.plot(u.reshape((numN, -1))); plt.title("u");
plt.subplot(5, 1, 3); plt.plot(v.reshape((numN, -1))); plt.title("v");
plt.subplot(5, 1, 4); plt.plot(b.reshape((numN, -1))); plt.title("b");
plt.subplot(5, 1, 5); plt.plot(d.reshape((numN, -1))); plt.title("d");
plt.xlabel("Epochs")



