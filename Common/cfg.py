#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 21:40:00 2021

@author: pimmostert
"""

from .empty import Empty

class Cfg(Empty):
    def ensure(self, name, default_value):
        if not hasattr(self, name):
            setattr(self, name, default_value)
        