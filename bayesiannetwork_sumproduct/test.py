import numpy as np

P1 = np.array([0.7, 0.3])
P2_1 = np.array([[0.1, 0.9], [0.2, 0.8]])
P3_1 = np.array([[0.6, 0.4], [1/3, 2/3]])
mu1_2 = np.array([[5/7, 2/7], [4/7, 3/7]])
mu2_2 = np.array([[2/9, 7/9], [5/9, 4/9]])
mu3_3 = np.array([[5/9, 4/9], [1/9, 8/9]])
mu4_3 = np.array([[4/11, 7/11], [2/11, 9/11]])

y1 = np.array([1, 0])
y2 = np.array([0, 1])
y3 = np.array([0, 1])
y4 = np.array([1, 0])

# [Q1, Q2, Q3, Y1, Y2, Y3, Y4]
p = P1[:, None, None, None, None, None, None] \
    * P2_1[:, :, None, None, None, None, None] \
    * P3_1[:, None, :, None, None, None, None] \
    * mu1_2[None, :, None, :, None, None, None] \
    * mu2_2[None, :, None, None, :, None, None] \
    * mu3_3[None, None, :, None, None, :, None] \
    * mu4_3[None, None, :, None, None, None, :] \
    * y1[None, None, None, :, None, None, None] \
    * y2[None, None, None, None, :, None, None] \
    * y3[None, None, None, None, None, :, None] \
    * y4[None, None, None, None, None, None, :]

c = p.sum()

p1 = p.sum(axis=(1, 2, 3, 4, 5, 6)) / c
p2 = p.sum(axis=(0, 2, 3, 4, 5, 6)) / c
p3 = p.sum(axis=(0, 1, 3, 4, 5, 6)) / c

p2_1 = p.sum(axis=(2, 3, 4, 5, 6)) / c
p3_1 = p.sum(axis=(1, 3, 4, 5, 6)) / c
m1_2 = p.sum(axis=(0, 2, 4, 5, 6)) / c
m2_2 = p.sum(axis=(0, 2, 3, 5, 6)) / c
m3_3 = p.sum(axis=(0, 1, 3, 4, 6)) / c
m4_3 = p.sum(axis=(0, 1, 3, 4, 5)) / c

# Sum-product algorithm        
a1 = y1
a3 = y2
a7 = y3
a9 = y4
a2 = (y1[None, :]*mu1_2).sum(axis=1)
a4 = (y2[None, :]*mu2_2).sum(axis=1)
a8 = (y3[None, :]*mu3_3).sum(axis=1)
a10 = (y4[None, :]*mu4_3).sum(axis=1)
a5 = a2 * a4
a11 = a8 * a10
a6 = (a5[None, :] * P2_1).sum(axis=1)
a12 = (a11[None, :] * P3_1).sum(axis=1)
a13 = a6 * a12

b1 = P1
b2 = a12 * b1
b8 = a6 * b1
b3 = (b2[:, None] * P2_1).sum(axis=0)
b9 = (b8[:, None] * P3_1).sum(axis=0)
b4 = a4 * b3
b6 = a2 * b3
b10 = a10 * b9
b12 = a8 * b9
b5 = (b4[:, None] * mu1_2).sum(axis=1)
b7 = (b6[:, None] * mu2_2).sum(axis=1)
b11 = (b10[:, None] * mu3_3).sum(axis=1)
b13 = (b12[:, None] * mu4_3).sum(axis=1)

pY = (b1 * a6 * a12).sum()

# P(Q1 | Y1, Y2, Y3, Y4)
print((a6 * a12 * b1) / pY)
print(p1)

# P(Q2 | Y1, Y2, Y3, Y4)
print((a2 * a4 * b3) / pY)
print(p2)

# P(Q1, Q2 | Y1, Y2, Y3, Y4)
print(b2[:, None] * P2_1 * a5[None, :] / pY)
print(p2_1)

# P(Q2, Y1 | Y1, Y2, Y3, Y4)
print(b4[:, None] * mu1_2 * a1[None, :] / pY)
print(m1_2)





