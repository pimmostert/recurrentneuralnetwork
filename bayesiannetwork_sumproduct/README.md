# Bayesian network, tree topology

Inference using the sum-product algorithm (see Bishop, 2006).

## Generative model

```mermaid
flowchart TD
    Q1((Q1))
    Q2((Q2))
    Q3((Q3))
    Y1((Y1))
    Y2((Y2))
    Y3((Y3))
    Y4((Y4))

    Q1-->Q2
    Q1-->Q3

    Q2-->Y1
    Q2-->Y2

    Q3-->Y3
    Q3-->Y4
```

```math
P(Q_1, Q_2, Q_3, Y_1, Y_2, Y_3, Y_4) = \\
    P(Q_1)P(Q_2 | Q_1)P(Q_3 | Q_1) \dots \\
    \dots P(Y_1 | Q_2)P(Y_2 | Q_2)P(Y_3 | Q_3)P(Y_4 | Q_3)
```

## Factor graph

```mermaid

graph TD
    Fa
    Q1((Q1))
    Q2((Q2))
    Q3((Q3))
    Y1((Y1))
    Y2((Y2))
    Y3((Y3))
    Y4((Y4))

    Fa--b1-->Q1
    Q1--a13-->Fa

    Fb--a6-->Q1
    Q1--b2-->Fb
    Q2--a5-->Fb
    Fb--b3-->Q2

    Fc--a12-->Q1
    Q1--b8-->Fc
    Q3--a11-->Fc
    Fc--b9-->Q3

    Fd--a2-->Q2
    Q2--b4-->Fd
    Y1--a1-->Fd
    Fd--b5-->Y1

    Fe--a4-->Q2
    Q2--b6-->Fe
    Y2--a3-->Fe
    Fe--b7-->Y2

    Ff--a8-->Q3
    Q3--b10-->Ff
    Y3--a7-->Ff
    Ff--b11-->Y3

    Fg--a10-->Q3
    Q3--b12-->Fg
    Y4--a9-->Fg
    Fg--b13-->Y4
```

```math
\begin{align}
    F_a(q_1) & = P(Q_1) \\
    F_b(q_1, q_2) & = P(Q_2 | Q_1) \\
    F_c(q_1, q_3) & = P(Q_3 | Q_1) \\
    F_d(q_2, y_1) & = P(Y_1 | Q_2) \\
    F_e(q_2, y_2) & = P(Y_2 | Q_2) \\
    F_f(q_3, y_3) & = P(Y_3 | Q_3) \\
    F_g(q_3, y_4) & = P(Y_4 | Q_3) \\
\end{align}
```
```math
P(Q_1, Q_2, Q_3, Y_1, Y_2, Y_3, Y_4) = \\
    F_a(q_1) F_b(q_1, q_2) F_c(q_1, q_3) \dots \\    
    \dots F_d(q_2, y_1) F_e(q_2, y_2) F_f(q_3, y_3) F_g(q_3, y_4)
```

## Inference

The goal is to find the marginals of hidden nodes, conditioned on the data, e.g. $`P(Q_2 | Y_1 = \hat{y}_1, Y_2 = \hat{y}_2, Y_3 = \hat{y}_3, Y_4 = \hat{y}_4)`$.

### Messages

```math
\begin{align}
    a_1(y_1) & =
        \begin{cases}
            1 & \text{if } y_1 = \hat{y}_1 \\
            0 & \text{if } y_1 \ne \hat{y}_1 \\
        \end{cases} \\

    a_2(q_2) & = \sum_{y_1} P(Y_1 | Q_2) \cdot a_1(y_1) & P(Y_1 | Q_2) \\

    a_3(y_2) & =
    \begin{cases}
        1 & \text{if } y_2 = \hat{y}_2 \\
        0 & \text{if } y_2 \ne \hat{y}_2 \\
    \end{cases} \\

    a_4(q_2) & = \sum_{y_2} P(Y_2 | Q_2) \cdot a_3(y_2) & P(Y_2 | Q_2) \\

    a_5(q_2) & = a_2(q_2) \cdot a_4(q_2) & P(Y_1, Y_2 | Q_2) \\

    a_6(q_1) & = \sum_{q_2} P(Q_2 | Q_1) \cdot a_5(q_2) & P(Y_1, Y_2 | Q_1) \\

    a_{12}(q_1) & = \sum_{q_3} P(Q_3 | Q_1) \cdot a_{11}(q_3) & P(Y_3, Y_4 | Q1) \\

    a_{13}(q_1) & = a_6(q_1) \cdot a_{12}(q_1) & P(Y_1, Y_2, Y_3, Y_4 | Q_1) \\

    b_1(q_1) & = P(Q_1) & P(Q_1)\\

    b_2(q_1) & = b_1(q_1) \cdot a_2(q_1) & P(Q_1, Y_3, Y_4) \\

    b_3(q_2) & = \sum_{q1} P(Q_2 | Q_1) \cdot b_2(q_1) & P(Q_2, Y_3, Y_4) \\

    b_4(q_2) & = b_3(q_2) \cdot a_4(q_2) & P(Q_2, Y_2, Y_3, Y_4) \\

    b_5(y_1) & = \sum_{q2} P(Y_1 | Q_2) \cdot b_4(q_2) & P(Y_1 = y_1, Y_2, Y_3, Y_4) \\

    b_6(q_2) & = b_3(q_1) \cdot a_2(q_2) & P(Q_2, Y_1, Y_3, Y_4) \\
\end{align}
```

### Marginals

```math
\begin{align}
    P(Q_1 | Y_1, Y_2, Y_3, Y_4) & =
        \frac
        {
            a_{13}(q_1) b_1(q_1)
        }
        { \displaystyle
            \sum_{q^*_1} a_{13}(q^*_1) b_1(q^*_1)
        } \\

    P(Q_2 | Y_1, Y_2, Y_3, Y_4) & =
        \frac
        {
            a_5(q_2) b_3(q_2)
        }
        { \displaystyle
            \sum_{q^*_2} a_5(q^*_2) b_3(q^*_2)
        } \\

    P(Q_1, Q_2 | Y_1, Y_2, Y_3, Y_4) & =
        \frac
        {
            a_5(q_2) P(Q_2 | Q_1) b_2(q_1)
        }
        { \displaystyle
            \sum_{q^*_1} \sum_{q^*_2} a_5(q^*_2) P(Q_2 | Q_1) b_2(q^*_1)
        } \\

    P(Q_2, Y_1 | Y_1, Y_2, Y_3, Y_4) & =
        \frac
        {
            a_1(y_1) P(Y_1 | Q_2) b_4(q_2)
        }
        { \displaystyle
            \sum_{q^*_2} \sum_{y^*_1} a_1(y^*_1) P(Q_2 | Q_1) b_4(q^*_2)
        }
\end{align}
```