import numpy as np
import common as c

class Network:
    def __init__(self, cfg0):
        self.numT = cfg0.numT
        self.numJ = cfg0.numJ
        self.numK = cfg0.numK
        
        self.w = cfg0.w0 if hasattr(cfg0, 'w0') else np.random.randn(cfg0.numJ, cfg0.numK) / cfg0.numJ
        self.b = cfg0.b0 if hasattr(cfg0, 'b0') else np.random.randn(cfg0.numK)
        self.v = cfg0.v0 if hasattr(cfg0, 'v0') else np.random.randn(cfg0.numK, cfg0.numK) / cfg0.numK
        self.u = cfg0.u0 if hasattr(cfg0, 'u0') else np.random.randn(cfg0.numK)
        self.d = cfg0.d0 if hasattr(cfg0, 'd0') else np.random.randn(1)
        
        self.fix_v = cfg0.fix_v if hasattr(cfg0, 'fix_v') else False
        
    def forward(self, x):
        self.a = np.zeros((self.numT, self.numK))
        self.h = np.zeros((self.numT, self.numK))
        self.y_hat = np.zeros((self.numT))
        
        self.a[0, :] = (self.w*x[0, :][:, None]).sum(axis=0) + self.b
        self.h[0, :] = c.sigmoid(self.a[0, :])
        self.y_hat[0] = (self.u * self.h[0, :]).sum() + self.d
        
        for it in np.arange(1, self.numT):
            self.a[it, :] = (self.w*x[it, :][:, None]).sum(axis=0) \
                + (self.v*self.h[it-1, :][:, None]).sum(axis=0) \
                + self.b
            self.h[it, :] = c.sigmoid(self.a[it, :])
            self.y_hat[it] = (self.u * self.h[it, :]).sum() + self.d
            
        log = c.Empty()
        log.y_hat = np.copy(self.y_hat)
        
        return log
            
    def update_weights(self, x, y, alpha):
        dh_db = np.zeros((self.numT, self.numK, self.numK))
        dh_dv = np.zeros((self.numT, self.numK, self.numK, self.numK))
        dh_dw = np.zeros((self.numT, self.numK, self.numJ, self.numK))

        dh_db[0, :, :] = np.diag(c.dsigmoid(self.a[0, :]))
        dh_dv[0, :, :, :] = 0
        dh_dw[0, :, :, :] = np.eye(self.numK, self.numK)[:, None, :] \
            *(c.dsigmoid(self.a[0, :])[:, None, None]*x[0, :][None, :, None])
        
        dsigmoid_a = c.dsigmoid(self.a)
        for it in np.arange(1, self.numT):
            dh_db[it, :, :] = \
                ((self.v * dh_db[it-1, :, :][:, None, :]).sum(axis=0) + np.eye(self.numK)) \
                * dsigmoid_a[it, :]
            dh_dv[it, :, :, :] = \
                ( \
                    (self.v[:, :, None, None] * dh_dv[it-1, :, :, :][:, None, :, :]).sum(axis=0)
                    + np.eye(self.numK, self.numK)[:, None, :]*self.h[it-1, :][None, :, None]
                ) \
                * dsigmoid_a[it, :]
            dh_dw[it, :, :, :] = \
                ( \
                    (self.v[:, :, None, None] * dh_dw[it-1, :, :, :][:, None, :, :]).sum(axis=0)
                    + np.eye(self.numK, self.numK)[:, None, :]*x[it, :][None, :, None]
                ) \
                * dsigmoid_a[it, :]
        
        delta = -(y - self.y_hat)
        
        dL_db_t = delta[:, None]*(self.u[None, :, None]*dh_db).sum(axis=1)
        dL_dv_t = delta[:, None, None]*(self.u[None, :, None, None]*dh_dv).sum(axis=1)                
        dL_dw_t = delta[:, None, None]*(self.u[None, :, None, None]*dh_dw).sum(axis=1)                
        dL_du_t = delta[:, None]*self.h
        dL_dd_t = delta

        # Calculate mean gradients over time
        dL_db = dL_db_t.mean(axis=0)
        dL_dv = dL_dv_t.mean(axis=0)
        dL_dw = dL_dw_t.mean(axis=0)
        dL_du = dL_du_t.mean(axis=0)
        dL_dd = dL_dd_t.mean(axis=0)
        
        # Update weights
        self.b -= alpha*dL_db
        self.w -= alpha*dL_dw
        self.u -= alpha*dL_du
        self.d -= alpha*dL_dd
        if not self.fix_v:
            self.v -= alpha*dL_dv
        
        log = c.Empty()
        log.b = np.copy(self.b)
        log.v = np.copy(self.v)
        log.w = np.copy(self.w)
        log.u = np.copy(self.u)
        log.d = np.copy(self.d)
        log.mse = (delta**2).mean()
        
        return log
        
        
        
        
        