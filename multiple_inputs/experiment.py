import common as c
import numpy as np

class Experiment:
    def __init__(self, network):
        self.network = network
        
    def run(self, cfg0, x, y):
        log = c.Empty()
        log.trials = [c.Empty() for _ in range(0, cfg0.numN)]
        
        for n in np.arange(0, cfg0.numN):
            log.trials[n].forward = self.network.forward(x)
            log.trials[n].update = self.network.update_weights(x, y, cfg0.alpha)
            
            if ((cfg0.feedback_epoch is not None) and (n % cfg0.feedback_epoch) == 0):
                print(f'Finished {n}/{cfg0.numN}')
                
        return log
        