# Simple recurrent neural network, multiple input

And multiple hidden nodes, fully connected.

## Model

```mermaid
 graph LR
    x((x1 ... xJ))
    h((h1 ... hK))
    y((y))

    x -- W --> h
    h -- V --> h
    h -- u --> y
```

```math 
\begin{align}
    a^t_k & = \sum_j w_{jk}x^t_j + \sum_{k'} v_{k'k} h^{t-1}_{k'} + b_k \\
    a^0_k & = \sum_j w_{jk}x^0 + b_k \\
    h^t_k & = \sigma(a^t_k) \\ 
    \hat{y}^t & = \sum_{k} u_k h^t_k + d
\end{align}
```

where $`\sigma`$ is the sigmoid function.

## Loss

Loss is defined as the mean squared error over all time samples.

```math 
\begin{align}
    L & = \frac{1}{T} \sum_{t=0}^T L^t \\
    L^t & = \frac{1}{2}(y^t - \hat{y}^t)^2
\end{align}
```

## Gradients
### $`d`$
<hr>

```math
\begin{align}
    \frac{\partial L^t}{\partial d} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial d} \\
    & = -(y^t - \hat{y}^t)
\end{align}
```

### $`u_i`$
<hr>

```math
\begin{align}
    \frac{\partial L^t}{\partial u_i} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial u_i} \\
    & = -(y^t - \hat{y}^t) h^t_i
\end{align}
```

### $`b_i`$
<hr>

```math 
\begin{align}
    \frac{\partial L^t}{\partial b_i} & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial b_i} \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k \frac{\partial}{\partial b_i}(u_kh^t_k) \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial b_i} \\
    & = \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial a_k} \cdot \frac{\partial a^t_k}{\partial b_i}
\end{align}
```

$`a^t_k`$ is dependent on $`b_i`$ both via $`b_k`$ itself (if $`k=i`$) as well as through $`h^{t-1}_k`$. Therefore:

```math 
\begin{align}
    \frac{\partial a^t_k}{\partial b_i} & = \sum_{k'} \frac{\partial}{\partial b_i}(v_{k'k} h^{t-1}_{k'}) + \frac{\partial b_k}{\partial b_i} \\
    \frac{\partial a^t_k}{\partial b_i} & = \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial b_i} + \frac{\partial b_k}{\partial b_i}
\end{align}
```

Putting it together:
```math 
\begin{align}
    \frac{\partial L^t}{\partial b_i} & = 
        -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial b_i} \\

    \frac{\partial h^t_k}{\partial b_i} & = 
        \begin{cases}
            \sigma'(a^t_k) 
            \left[ 
                \sum_{k'}v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial b_i} + 1
            \right], & \text{if } k = i \\

            \sigma'(a^t_k) 
            \left[ 
                \sum_{k'}v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial b_i}
            \right], & \text{if } k \ne i \\
        \end{cases} \\

    \frac{\partial h^0_k}{\partial b_i} & = 
        \begin{cases}
            \sigma'(a^0_k)
                , & \text{if } k = i \\

            0
                , & \text{if } k \ne i \\
        \end{cases} \\
\end{align}
```

### $`v_{li}`$
<hr>

Analogously to $`b_i`$:

```math 
\begin{align}
    \frac{\partial L^t}{\partial v_{li}} & = 
        \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial v_{li}} \\

    & = 
        \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial a_k} \cdot \frac{\partial a^t_k}{\partial v_{li}}
\end{align}
```

However calculation of $`\frac{\partial a^t}{\partial v_{li}}`$ now requires the product rule:

```math 
\begin{align}
    \frac{\partial a^t_k}{\partial v_{li}} = & 
        \sum_{k'} \frac{\partial}{\partial v_{li}} ( v_{k'k} h^{t-1}_{k'} ) \\

    \frac{\partial a^t}{\partial v_{li}} = & 
        \sum_{k'} 
        \left [
            v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{li}} 
            + \frac{\partial v_{k'k}}{\partial v_{li}} h^{t-1}_{k'}
        \right ] \\

    = & 
        \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{li}} 
        + \sum_{k'} \frac{\partial v_{k'k}}{\partial v_{li}} h^{t-1}_{k'}
\end{align}
```

The second summation can be simplified. First, expand it:

```math 
\sum_{k'} \frac{\partial v_{k'k}}{\partial v_{li}} h^{t-1}_{k'} =
    \frac{\partial v_{1k}}{\partial v_{li}} h^{t-1}_1
    + \frac{\partial v_{2k}}{\partial v_{li}} h^{t-1}_2
    + ...
    + \frac{\partial v_{lk}}{\partial v_{li}} h^{t-1}_{l}
    + ...
    + \frac{\partial v_{Kk}}{\partial v_{li}} h^{t-1}_{K}
```

All partial derivatives are zero, except potentially $`\frac{\partial v_{lk}}{\partial v_{li}}`$, which is 1 if $`i = k`$ and 0 otherwise. Hence:

```math 
\begin{align}
    \sum_{k'} \frac{\partial v_{k'k}}{\partial v_{li}} h^{t-1}_{k'} = &
        \frac{\partial v_{lk}}{\partial v_{li}} h^{t-1}_{l} \\

    = &
        \begin{cases}
            h^{t-1}_{l}
                , & \text{if } k = i \\
                
            0
                , & \text{if } k \ne i \\
        \end{cases} \\
\end{align}
```

Together:

```math 
\begin{align}
    \frac{\partial L^t}{\partial v_{li}} & = 
        -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial v_{li}} \\

    \frac{\partial h^t_k}{\partial v_{li}} & =
        \begin{cases}
            \sigma'(a^t_k)
            \sum_{k'} 
            \left [
                v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{li}} 
                + h^{t-1}_l
            \right ]
                , & \text{if } k = i \\
                
            \sigma'(a^t_k)
            \sum_{k'} 
            v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial v_{li}}
                , & \text{if } k \ne i \\
        \end{cases} \\

    \frac{\partial h^0_k}{\partial v_{li}} & =
        \sigma'(a^t_k) \cdot 0 = 0 \\
        
\end{align}
```

### $`w_{li}`$
<hr>

Analogously to $`v_{li}`$:

```math 
\begin{align}
    \frac{\partial L^t}{\partial w_{li}} & = 
        \frac{\partial L^t}{\partial \hat{y}^t} \cdot \frac{\partial \hat{y}^t}{\partial w_{li}} \\

    & = 
        \frac{\partial L^t}{\partial \hat{y}^t} \cdot \sum_k u_k \frac{\partial h^t_k}{\partial a_k} \cdot \frac{\partial a^t_k}{\partial w_{li}}
\end{align}
```

```math 
\begin{align}
    \frac{\partial a^t_k}{\partial w_{li}} = & 
         \frac{\partial}{\partial w_{li}} \sum_{j} ( w_{jk} x^{t}_j ) 
         + \frac{\partial}{\partial w_{li}} \sum_{k'} ( v_{k'k} h^{t-1}_{k'} ) \\

    = & 
         \sum_{j} x^{t}_j \frac{\partial w_{jk}}{\partial w_{li}} 
         + \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial w_{li}} \\
\end{align}
```

The first summation can be simplified analogously to above for $`v_{li}`$:

```math 
\begin{align}
    \sum_{j} x^{t}_j \frac{\partial w_{jk}}{\partial w_{li}} = &
        \begin{cases}
            x^t_l
                , & \text{if } k = i \\
                
            0
                , & \text{if } k \ne i \\
        \end{cases} \\
\end{align}
```

Together:

```math 
\begin{align}
    \frac{\partial L^t}{\partial w_{li}} & = 
        -(y^t - \hat{y}^t) \sum_k u_k \frac{\partial h^t_k}{\partial w_{li}} \\

    \frac{\partial h^t_k}{\partial w_{li}} & =
        \begin{cases}
            \sigma'(a^t_k)
            \left [
                \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial w_{li}}
                + x^t_l
            \right ]
                , & \text{if } k = i \\
                
            \sigma'(a^t_k)
            \sum_{k'} v_{k'k} \frac{\partial h^{t-1}_{k'}}{\partial w_{li}}
                , & \text{if } k \ne i \\
        \end{cases} \\

    \frac{\partial h^0_k}{\partial w_{li}} & =
        \begin{cases}
            \sigma'(a^0_k)
            x^0_l
                , & \text{if } k = i \\
                
            0
                , & \text{if } k \ne i \\
        \end{cases} \\
        
\end{align}
```

