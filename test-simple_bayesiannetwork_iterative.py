from simple_bayesiannetwork_stochastic import *
import numpy as np
import common as c
import matplotlib.pyplot as plt

# Generate data
numN = 20000

cfg = c.Cfg()
cfg.A1 = np.array([0.2, 0.8])
cfg.A2 = np.array([[0.7, 0.3], [0.5, 0.5]])
cfg.A3 = np.array([[[0.1, 0.9], [1/3, 2/3]], [[1/3, 2/3], [0.9, 0.1]]])
cfg.mu = np.eye(8).reshape((2, 2, 2, 8))

model0 = Model(cfg)
X = model0.generate_data(numN)

# Train new model
cfg = c.Cfg()
cfg.A1 = generate_random_probability_matrix((2))
cfg.A2 = generate_random_probability_matrix((2, 2))
cfg.A3 = generate_random_probability_matrix((2, 2, 2))
cfg.mu = np.random.random((2, 2, 2, 8))
# cfg.A1 = model0.A1
# cfg.A2 = model0.A2
# cfg.A3 = model0.A3
# cfg.mu = model0.mu * 0.999999 + 0.0000005

model = Model(cfg)

gamma = 0.002

log = c.Empty()
log.ll = np.zeros(numN)
log.m_ll = np.zeros(numN)
log.A1 = np.zeros((numN, 2))
log.A2 = np.zeros((numN, 2, 2))
log.A3 = np.zeros((numN, 2, 2, 2))
log.mu = np.zeros((numN, 2, 2, 2, 8))

for nn in range(0, numN):
    log.A1[nn, :] = model.A1
    log.A2[nn, :, :] = model.A2
    log.A3[nn, :, :, :] = model.A3
    log.mu[nn, :, :, :, :] = model.mu

    x = X[nn, :]
    (p, ll) = model.infer(x)
    log.ll[nn] = ll
    log.m_ll[nn] = log.ll[max(0, nn-100):(nn+1)].mean()

    model.learn(p, x, gamma)
    
    
plt.figure()
plt.plot(log.ll); plt.xlim(0, numN)
plt.plot(log.m_ll, color='black');

plt.figure()
plt.subplot(4, 1, 1); plt.plot(log.A1.reshape((numN, -1))); plt.ylim([0, 1]); plt.xlim(0, numN)
plt.subplot(4, 1, 2); plt.plot(log.A2.reshape((numN, -1))); plt.ylim([0, 1]); plt.xlim(0, numN)
plt.subplot(4, 1, 3); plt.plot(log.A3.reshape((numN, -1))); plt.ylim([0, 1]); plt.xlim(0, numN)
plt.subplot(4, 1, 4); plt.plot(log.mu.reshape((numN, -1))); plt.ylim([0, 1]); plt.xlim(0, numN)

plt.figure()
plt.pcolormesh(model.mu.reshape((-1, 8)), vmin=0, vmax=1)

plt.figure(figsize=(15, 10))
plotdata = log.mu.reshape([numN, 8, 8])
for i in range(0, 8):
    plt.subplot(8, 1, i+1)
    plt.pcolormesh(plotdata[:, i, :].T, vmin=0, vmax=1)
    







