# Simple Bayesian network

## Model

```mermaid
graph TD
    Q1((Q1))
    Q2((Q2))
    Q3((Q3))
    x((x))

    Q1-->Q2
    Q2-->Q3
    Q3-->x
    
    Q2-->x
    Q1-->x
    Q1-->Q3
```

The full model is given by:
```math
    P(Q^1, Q^2, Q^3, \mathbf{x}) = P(Q^1)P(Q^2|Q^1)P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3)
```

In this document I define each node $`Q^i`$ as a categorical variable. The associated conditional probability distributions (CPD) are simple conditional probability tables (CPTs):

```math 
\begin{align}
    & P(Q^1_l = 1) & = A^1_{l} \\
    & P(Q^2_i = 1 | Q^1_l = 1) &= A^2_{li} \\
    & P(Q^3_j = 1 | Q^1_l = 1, Q^2_i = 1) & = A^3_{lij} \\
\end{align}
```

$`\mathbf{x}`$ is a $`K`$-dimensional vector and follows a Bernoulli distribution, conditioned on all $`Q`$:

```math
\begin{array}{lclcl}
    P(x_k = 1 & | & Q^1_l=1, Q^2_i=1, Q^3_j=1) & = & 
        \mu_{lijk} \\ \\

    P(x_k & | & Q^1_l=1, Q^2_i=1, Q^3_j=1) & = & 
        \mu_{lijk}^{x_k} (1-\mu_{lijk})^{1-x_k} \\ \\

    P(\mathbf{x} & | & Q^1_l=1, Q^2_i=1, Q^3_j=1) & = & 
        \displaystyle \prod_{k} \mu_{lijk}^{x_k} (1-\mu_{lijk})^{1-x_k} \\

    \ln P(\mathbf{x} & | & Q^1_l=1, Q^2_i=1, Q^3_j=1) & = & 
        \displaystyle \sum_{k} 
        \left [
            x_k \ln{\mu_{lijk}} + (1-x_k) \ln{(1-\mu_{lijk})} 
        \right ] \\

    & & & = & 
        \displaystyle \sum_{k} 
            x_k \ln{\frac{\mu_{lijk}}{1-\mu_{lijk}}} + \sum_{k} \ln{(1-\mu_{lijk})} \\
\end{array}
```

## Inference (E-step)

```math
\begin{array}{lcl}
    P(Q^1,Q^2,Q^3 | \mathbf{x}) & = & 
        \displaystyle \frac{P(Q^1,Q^2,Q^3, \mathbf{x})}{P(\mathbf{x})} \\ \\

    & = & \alpha P(Q^1,Q^2,Q^3, \mathbf{x}) \\ \\

    & = & \alpha P(Q^1)P(Q^2|Q^1)P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3) \\
\end{array}
```

```math
\begin{array}{lcl}
    P(Q^1 | \mathbf{x}) & = & 
        \displaystyle \frac{P(Q^1, \mathbf{x})}{P(\mathbf{x})} \\ \\

    & = & \displaystyle \alpha \sum_{Q^2} \sum_{Q^3} P(Q^1,Q^2,Q^3, \mathbf{x}) \\ \\

    & = & \displaystyle \alpha P(Q^1) \sum_{Q^2} P(Q^2|Q^1) \sum_{Q^3} P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3) \\
\end{array}
```

```math
\begin{array}{lcl}
    P(Q^2 | \mathbf{x}) & = & 
        \displaystyle \alpha \sum_{Q^1} P(Q^1) P(Q^2|Q^1) \sum_{Q^3} P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3) \\
\end{array}
```

```math
\begin{array}{lcl}
    P(Q^3 | \mathbf{x}) & = & 
        \displaystyle \alpha \sum_{Q^1} P(Q^1) \sum_{Q^2} P(Q^2|Q^1)  P(Q^3|Q^1,Q^2)P(\mathbf{x}|Q^1,Q^2,Q^3) \\
\end{array}
```

## Learning (M-step)

```math
\begin{array}{lcl} 
    A^1_l & = & 
         \displaystyle \frac{1}{N} \sum_{n} P(Q^1_l = 1 | \mathbf{x}_n) \\
    
    & = & 
         \displaystyle \frac{1}{N} \sum_{n} \sum_{Q^2} \sum_{Q^3} P(Q^1_l = 1,Q^2,Q^3 | \mathbf{x}_n) \\
\end{array}
```

```math
\begin{array}{lcl} 
    A^2_{li} & = & 
         \displaystyle \frac{1}{N} \sum_{n} P(Q^2_i = 1 | Q^1_l = 1, \mathbf{x}_n) \\
    
    & = & 
        \displaystyle \alpha \frac{1}{N} \sum_{n} 
        P(Q^2_i = 1, Q^1_l = 1 | \mathbf{x}_n) \\

    & = & 
         \displaystyle \alpha \frac{1}{N} \sum_{n}
            \displaystyle \sum_{Q^3} P(Q^2_i = 1, Q^1_l = 1, Q^3 | \mathbf{x}_n)
\end{array}
```

where $`\alpha`$ is a normalization constant such that $`\sum_i A^2_{li} = 1`$.

```math
\begin{array}{lcl} 
    A^3_{lij} & = & 
        \displaystyle \frac{1}{N} \sum_{n} P(Q^3_j = 1 | Q^1_l = 1, Q^2_i = 1, \mathbf{x}_n) \\
    
    & = & 
        \displaystyle \alpha \frac{1}{N} \sum_{n} 
        P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1 | \mathbf{x}_n) \\
\end{array}
```

where $`\alpha`$ is a normalization constant such that $`\sum_j A^3_{lij} = 1`$.

```math
\begin{array}{lcl}
    \mu_{lijk} & = & 
        \displaystyle \frac
        {
            \displaystyle \frac{1}{N}
            \sum_N P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1 | \mathbf{x}_n) \mathbf{x}_{nk} 
        }
        {
            \displaystyle \frac{1}{N}
            \sum_N P(Q^1_l = 1, Q^2_i = 1, Q^3_j = 1 | \mathbf{x}_n) 
        }
\end{array}
```

