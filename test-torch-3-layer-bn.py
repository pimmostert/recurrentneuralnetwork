from simple_bayesiannetwork_torch import *
import numpy as np
import common as c
import matplotlib.pyplot as plt
torch.set_default_dtype(torch.float64)

# Generate data
cfg = c.Cfg()
cfg.A1 = torch.tensor([0.5, 0.5], device=c.torch_device)
cfg.A2 = torch.tensor([[0.5, 0.5], [0.5, 0.5]], device=c.torch_device)
cfg.A3 = torch.tensor([[[0.5, 0.5], [0.5, 0.5]], [[0.5, 0.5], [0.5, 0.5]]], device=c.torch_device)
cfg.mu = torch.eye(8, device=c.torch_device).reshape((2, 2, 2, 8))

model0 = Model(cfg)
X = model0.generate_data(10000)

# Train new model
cfg = c.Cfg()
cfg.A1 = generate_random_probability_matrix((2))
cfg.A2 = generate_random_probability_matrix((2, 2))
cfg.A3 = generate_random_probability_matrix((2, 2, 2))
cfg.mu = torch.rand((2, 2, 2, 8))

model = Model(cfg)

numEpochs = 5000

log = c.Empty()
log.ll = torch.zeros(numEpochs, device=c.torch_device)
log.A1 = torch.zeros((numEpochs, 2), device=c.torch_device)
log.A2 = torch.zeros((numEpochs, 2, 2), device=c.torch_device)
log.A3 = torch.zeros((numEpochs, 2, 2, 2), device=c.torch_device)
log.mu = torch.zeros((numEpochs, 2, 2, 2, 8), device=c.torch_device)

for iEpoch in range(0, numEpochs):
    log.A1[iEpoch, :] = model.A1
    log.A2[iEpoch, :, :] = model.A2
    log.A3[iEpoch, :, :, :] = model.A3
    log.mu[iEpoch, :, :, :, :] = model.mu

    (p, ll) = model.infer(X)
    log.ll[iEpoch] = ll

    model.learn(p, X)

    if ((iEpoch % 1000) == 0):
        print(f'Finished epoch {iEpoch}/{numEpochs}')

plt.figure()
plt.subplot(2, 1, 1);
plt.plot(log.ll);
plt.xlim(0, numEpochs)
plt.subplot(2, 1, 2);
plt.plot(log.ll[1:] >= log.ll[:-1]);
plt.xlim(0, numEpochs)

plt.figure()
plt.subplot(4, 1, 1);
plt.plot(log.A1.reshape((numEpochs, -1)));
plt.ylim([0, 1])
plt.subplot(4, 1, 2);
plt.plot(log.A2.reshape((numEpochs, -1)));
plt.ylim([0, 1])
plt.subplot(4, 1, 3);
plt.plot(log.A3.reshape((numEpochs, -1)));
plt.ylim([0, 1])
plt.subplot(4, 1, 4);
plt.plot(log.mu.reshape((numEpochs, -1)));
plt.ylim([0, 1])

plt.figure()
plt.pcolormesh(model.mu.reshape((-1, 8)), vmin=0, vmax=1)

plt.show()
